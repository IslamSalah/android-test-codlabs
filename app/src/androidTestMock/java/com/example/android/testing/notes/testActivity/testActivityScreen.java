package com.example.android.testing.notes.testActivity;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.android.testing.notes.R;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by Islam Salah on 1/10/18.
 * <p>
 * https://github.com/IslamSalah
 * islamsalah007@gmail.com
 */
@RunWith(AndroidJUnit4.class)
public class testActivityScreen {

    @Rule
    public ActivityTestRule<MyActivity> mActivityTestRule = new ActivityTestRule<>(MyActivity.class);

    @Test
    public void foo(){
        onView(withId(R.id.fl)).perform(scrollTo())
        .check(matches(isCompletelyDisplayed()));
    }
}
